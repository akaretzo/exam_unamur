import random
import csv


PRICE_HIGHEST = 10

PRICE_FRAPPUCINO = 4
PRICE_SODA = 3
PRICE_MILKSHAKE = 5
PRICE_WATER = 2
PRICE_TEA = 3
PRICE_COFFEE = 3

PRICE_SANDWICH = 5
PRICE_MUFFIN = 3
PRICE_COOKIE = 2
PRICE_PIE = 3

#Returns an element of l on basis of the probabilities of p
def choose(l, p):
    r = random.uniform(0,1)
    total=0
    for i in range(0,len(l)):
        total = total + float(p[i])
        if (total >= r):
            return l[i]

#Returns the price of name
def getPrice(name):
    #drinks
    if (name == "frappucino"):
        return PRICE_FRAPPUCINO
    elif (name == "soda"):
        return PRICE_SODA
    elif (name == "milkshake"):
        return PRICE_MILKSHAKE
    elif (name == "water"):
        return PRICE_WATER
    elif (name == "coffee"):
        return PRICE_COFFEE
    elif (name == "tea"):
        return PRICE_TEA
    #food
    elif (name == "sandwich"):
        return PRICE_SANDWICH
    elif (name == "muffin"):
        return PRICE_MUFFIN
    elif (name == "cookie"):
        return PRICE_COOKIE
    elif(name == "pie"):
        return PRICE_PIE
    else:
        return 0
class Customer:
    CID = 0;

    # customerType = ["regOnce", "tripadvisor", "regReturn", "hipster" ]
    def __init__(self, customerType):
        self.customerType = customerType
        self.budget = self.budgetCalc()
        self.logFood = []
        self.logDrinks = []
        self.logBudget = [self.budget]
        self.history = []
        self.banned = False
        self.CID = Customer.CID
        Customer.CID = Customer.CID + 1


    def budgetCalc(self):
        if self.customerType is "hipster":
            return 500
        elif self.customerType is "regReturn":
            return 250
        else:
            return 100

    def tip(self):
        if self.customerType is not "tripadvisor":
            return 0
        else:
            return random.randrange(1,10)

    #This function is called when buying a drink, it logs everything and sets the new budget of the customer
    def buyDrink(self, time, prct):
        with open("prbDrinks.txt") as f:
            reader = csv.reader(f, delimiter=";")
            choice = ""
            for i, line in enumerate(reader):
                if (line[0] == time):
                    l = line[1].split(',')
                    p = line[2].split(',')
                    choice = choose(l,p)
                    self.logDrinks.append(choice)
                    self.budget = self.budget - getPrice(choice) - prct*getPrice(choice) - self.tip()
                    self.logBudget.append(self.budget)
                    self.history.append("Bought " + str(choice) + " for " + str(getPrice(choice) + prct*getPrice(choice) + self.tip()) + " euro at " + str(time))
                    break
    #This function is called when buying a food, it logs everything and sets the updated budget of the customer
    def buyFood(self, time, prct):
        with open("prbFood.txt") as f:
            reader = csv.reader(f, delimiter=";")
            choice = ""
            for i, line in enumerate(reader):
                if (line[0] == time):
                    l = line[1].split(',')
                    p = line[2].split(',')
                    choice = choose(l,p)
                    self.logFood.append(choice)
                    self.budget = self.budget - getPrice(choice) - prct*getPrice(choice) - self.tip()
                    self.logBudget.append(self.budget)
                    self.history.append("Bought " + str(choice) + " for " + str(getPrice(choice) + prct*getPrice(choice) + self.tip()) + " euro at " + str(time) + "\n")
                    break
        if (self.budget < PRICE_HIGHEST):
            self.ban()
    def tellDrinks(self):
        s=""
        if (len(self.logDrinks)>0):
            s = s + "\n" + str(self.logDrinks[len(self.logDrinks) - 1])
        else:
            s = s + "\n" + ""
        return s
    def tellFood(self):
        s="\n"
        if (len(self.logFood)>0):
            s = s + "\n" + str(self.logFood[len(self.logFood) - 1])
        else:
            s = s + "\n" + ""
        return s
    def tellLastFood(self):
        return self.logFood[len(self.logFood) - 1]
    def tellLastDrink(self):
        return self.logDrinks[len(self.logDrinks) - 1]
    def tellBudget(self):
        s = ""
        for i in range(0,len(self.logBudget)):
            s = s + "\n" + str(self.logBudget[i])+" euro"
        return s

    def tellHistory(self):
        s = ""
        for i in range(0, len(self.history)):
             s = s + "\n\t" + str(self.history[i])
        return s
    def ban(self):
        self.banned = True
    def isBanned(self):
        return self.banned
"""
#TESTS
cust1 = Customer("hipster")
cust2 = Customer("tripadvisor")
cust3 = Customer("regOnce")
cust4 = Customer("hipster")
cust1.buyDrink("12:00:00")
print(cust1.budget)
cust1.buyFood("11:00:00")

cust1.tellDrinks()
cust1.tellFood()
cust1.tellBudget()
"""
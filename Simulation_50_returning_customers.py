import Customers as cust
import csv
import random
bannedReturningCustomers = 0 #list of indexes of returning customers with no more budget
bannedReturningCustomersIndex = []

#Creating the returning customers
returningCustomers = []
for i in range(0,49):
    if (i <= 16):
        newCustomer = cust.Customer("hipster") #Creating hipsters
        returningCustomers.append(newCustomer)
    else:
        newCustomer = cust.Customer("regReturn") #creating regular returning customers (called regReturn)
        returningCustomers.append(newCustomer)

#Going through every timestep of the file and simulating behaviour
with open("Coffeebar_2013-2017.csv") as f:
    next(f)
    reader = csv.reader(f, delimiter=";")

    df = open("simulations/simulation_50_returning_customers_final.csv", 'a+')
    df.write("TIME;CUSTOMER;DRINKS;FOOD\n")
    for i, line in enumerate(reader):
        CID = 0
        drinked = ""
        eaten = ""
        time = line[0].split(' ')[1]
        if(cust.choose(['returning','once'],[0.2,0.8]) == 'returning') and (bannedReturningCustomers<49):
            #returning customer
            valid_indexes = list(set(range(0, 49)) - set(bannedReturningCustomersIndex))
            index = random.choice(valid_indexes)
            # checking if returning customer is banned


            while returningCustomers[index].isBanned():
                print("Banned Returning Customer:"+ str(index) +" Total:" + str(bannedReturningCustomers))
                index = random.randrange(0,49)
                index = next(iter(set(range(0,49)) - set(bannedReturningCustomersIndex)))


            returningCustomers[index].buyFood(time, 0)
            returningCustomers[index].buyDrink(time, 0)
            drinked = returningCustomers[index].tellLastDrink()
            eaten = returningCustomers[index].tellLastFood()
            CID = returningCustomers[index].CID

            if  returningCustomers[index].isBanned() and (index not in bannedReturningCustomersIndex):
                bannedReturningCustomers = bannedReturningCustomers + 1
                bannedReturningCustomersIndex.append(index)
                print("Banning Returning Customer:" + str(index) + " Total:" + str(bannedReturningCustomers))

        else:
            #one-timer customer
            customerType = cust.choose(['tripadvisor','regOnce'],[0.1,0.9])
            oneTimeCustomer = cust.Customer(customerType)
            oneTimeCustomer.buyDrink(time, 0)
            oneTimeCustomer.buyFood(time, 0)
            drinked = oneTimeCustomer.tellLastDrink()
            eaten = oneTimeCustomer.tellLastFood()
            CID = oneTimeCustomer.CID
        if (str(eaten)=="None"):
            eaten = ""
        if (str(drinked)=="None"):
            drinked = ""
        df.write(line[0] + ";" + str(CID) + ";" + str(drinked) + ";" + str(eaten) + "\n")
        print(i)
    print("End of simulation when only 50 returning customers")
    df.close()

#writing some buying history of 10 random returning customers to outputfile
print("Writing history file")
buyingHistoryFile = open("buying_history_50_returning_customers_final.txt","a+")
buyingHistoryFile.write("This file contains the buying history of 10 random returning customers of the 50\n")
i = 0
while (i<10):
    ind = random.randrange(0,49)
    buyingHistoryFile.write("\n("+str(i+1)+") CustomerID: "+ str(ind) +"\t history:\n \t"+ returningCustomers[ind].tellHistory())
    i = i+1
buyingHistoryFile.close()
print("History file written")
